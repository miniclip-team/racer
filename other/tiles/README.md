# create custom map from google map tiles


1. create for stylized version of google maps you want

visit this web app: <http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html>

after adding the rules to remove labels and paint roads gray, click on the static map button

save the generated URL. style argument will be used in the script below


2. fetch tiles to disk and merge them

edit relevant configuration there

    node fetchTilesAndMergeThem.js


3. round to multiple of 256 and retile

    node splitIntoNewTiles.js


example results:

the merged file `merged.png`
the merged and cropped file `merged2.png`
170 tiles named `tile_0` to `tile_169`
    
15x18 tiles of 256x256
    
      0... 14
     15... 29
    .........
    255...269
    