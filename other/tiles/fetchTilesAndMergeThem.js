/*
  2015, José Pedro Dias <jose.pedro.dias@gmail.com>

  fetches several google map tiles in series, saving them to disk in a grid file pattern

  also does a mosaic with the result, but ended up using the `overlay.sh` solution instead


  Usage:

    edit configuration variables below

    <sudo> npm install
    node index.js

    TODO: detect bad responses, sleep, try again
 */



var fs = require('fs');

var request = require('request');
var async = require('async');
var gm = require('gm');



//http://maps.googleapis.com/maps/api/staticmap?center=38.733158,-9.303585&zoom=17&format=png&sensor=false&size=512x512&maptype=hybrid&style=feature:road.arterial|element:geometry|visibility:on|color:0x808080&style=feature:road.local|color:0x808080&style=feature:landscape.man_made|color:0x6ed17c&style=feature:road.arterial|element:labels|visibility:off&style=feature:road.arterial|element:geometry.stroke|color:0xfffffd&style=feature:road.highway|element:geometry.fill|color:0x808080&style=feature:poi|element:labels.text|visibility:off&style=feature:administrative|element:labels.text|visibility:off
//var TEMPLATE_URL = 'http://maps.googleapis.com/maps/api/staticmap?center={CENTER}&zoom={ZOOM}&format=png&sensor=false&size={SIZEW}x{SIZEH}&maptype=hybrid&style={STYLE}';
var TEMPLATE_URL = 'http://maps.googleapis.com/maps/api/staticmap?center={CENTER}&zoom={ZOOM}&format=png&sensor=false&size={SIZEW}x{SIZEH}&maptype=satellite';



// CONFIGURATION

var tileFilePrefix  = 'one/tile';
var mergeFilePrefix = 'one/merged';
var suffix = '.png';

var style = 'feature:road.arterial|element:geometry|visibility:on|color:0x808080&style=feature:road.local|color:0x808080&style=feature:landscape.man_made|color:0x6ed17c&style=feature:road.arterial|element:labels|visibility:off&style=feature:road.arterial|element:geometry.stroke|color:0xfffffd&style=feature:road.highway|element:geometry.fill|color:0x808080&style=feature:poi|element:labels.text|visibility:off&style=feature:administrative|element:labels.text|visibility:off';
var center = [38.7366258,-9.3046217];
var zoom = 18;
//var zoom = 19;
var size = 512;
var delta = 0.002;
//var delta = 0.001;
var dTile = 5;
var sleepBetweenFetches = 0.1; // in seconds
var grid = [
  [-dTile,  dTile], //vert
  [ dTile, -dTile]  //hor
];



var repl = function(s, key, val) {
  return s.replace('{'+key+'}', val);
};

var repls = function(s, o) {
  var k, v;
  for (k in o) {
    v = o[k];
    s = repl(s, k, v);
  }
  return s;
};

var fetchFile = function(opts, cb) {
  var o = {
    CENTER : opts.center.join(','),
    ZOOM   : zoom,
    SIZEW  : size,
    SIZEH  : size,
    STYLE  : style
  };
  var url = repls(TEMPLATE_URL, o);
  
  var w = fs.createWriteStream(opts.filename);
  w.on('finish', function(err) {
    if (err) { return cb(err); }
    // return cb(null);
    console.log('sleeping for ' + sleepBetweenFetches + 's...');
    setTimeout(function() {
      cb(null);
    }, sleepBetweenFetches*1000);
  });
  
  request
  .get(url)
  .on('error', cb)
  .pipe(w);
};



var filenameGen = function(filePrefix, y, x, suffix) {
  return [filePrefix, '_', y, '_', x, suffix].join('');
};


var generateItems = function(center, grid, delta, filePrefix, suffix) {
  var x, y, items = [];
  for (y = grid[0][0]; y < grid[0][1]; ++y) {
    for (x = grid[1][0]; x >= grid[1][1]; --x) {
      items.push({
        center: [
          center[0] - y*delta,
          center[1] - x*delta
        ],
        filename: filenameGen(filePrefix, y, x, suffix)
      });
    }
  }
  return items;
};


var generateItems2 = function(grid) {
  var y, yy = [];
  for (y = grid[0][0]; y < grid[0][1]; ++y) {
    yy.push(y);
  }
  
  var x, xx = [];
  for (x = grid[1][0]; x >= grid[1][1]; --x) {
    xx.push(x);
  }
  
  return {yy:yy, xx:xx};
};



var mergeTiles = function(cb) {
  console.log('about to merge the tiles');
  var g = gm();
  var items = generateItems2(grid);
  
  // create gm process...
  var szW = size - 140;
  var szH = size -  33; //32

  items.yy.forEach(function(y, iy) {
    items.xx.forEach(function(x, ix) {
      g = g
      .in('-page', ['+', ix*szW, '+', iy*szH].join(''))
      .in( filenameGen(tileFilePrefix, y, x, suffix) );
    });
  });
  
  // run it!
  g
  .mosaic()
  .write(mergeFilePrefix + suffix, cb);
};



var fetchTiles = function(cb) {
  var items = generateItems(center, grid, delta, tileFilePrefix, suffix);
  async.eachSeries(
    items, // items do process
    function(item, cb) { // iterator
      console.log('fetching %s to %s...', item.center.join(','), item.filename);
      fetchFile(item, function(err) {
        if (err) { return cb(err); }
        console.log('stored %s on %s.', item.center.join(','), item.filename);
        cb(null);
      });
    },
    function(err) { // on error/completion
      if (err) { return cb(err); }
      cb(null);
    }
  );
};



fetchTiles(function(err) {
  if (err) { throw err; }
  //console.log('ALL DONE');

  mergeTiles(function(err) {
    if (err) { throw err; }
    console.log('ALL DONE');
  });

});
