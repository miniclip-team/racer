var child_process = require('child_process');



// configuration
var tileSize = 256;
var inFile = 'two/merged.png';
var croppedFile = 'two/merged2.png';
var outTiles = 'two/tile_%d.png';



var int = function(s) { return parseInt(s, 10); };



// 1. get size
var inS = ['identify ', inFile].join('');
console.log('> ' + inS);
var outS = child_process.execSync(inS).toString();

var dims = outS.split(' ')[2].split('x');
dims = dims.map(int);
console.log('dims:', dims);



// 2. convert to nearest multiple of 256

var tiles = dims.map(function(n) {
    return Math.floor( n / tileSize );
});

var dims2 = tiles.map(function(n) {
    return n * tileSize;
});

console.log('tiles:', tiles);
console.log('dims2:', dims2);



// 3. crop
inS = ['convert -crop ', dims2[0], 'x', dims2[1], '+0+0 ', inFile, ' ', croppedFile].join('');
console.log('> ' + inS);
/*outS =*/ child_process.execSync(inS).toString();



// 4. create tiles
inS = ['convert +repage -crop ', tileSize, 'x', tileSize, ' ', croppedFile, ' ', outTiles].join('');
console.log('> ' + inS);
/*outS =*/ child_process.execSync(inS).toString();


var nrTiles = tiles[0] * tiles[1];
console.log('\ncreated tiles from:');
console.log( outTiles.replace('%d', '0') );
console.log('to:');
console.log( outTiles.replace('%d', (nrTiles-1).toString() ) );
