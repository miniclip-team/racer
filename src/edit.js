(function(w) {

    'use strict';


    var Vec = w.SSCD.Vector;



    // state
    var point = new Vec(0, 0);
    var strokeStartScroll;
    var lastId = 'car';



    var inp = w.input;

    var W = window.innerWidth;
    var H = window.innerHeight;
    var GAP = 16;

    var D_ROT = 5;

    var gui = {};


    var eng = getEngine({
        dims     : [W, H],
        gridSize : 1024,
        debugCollisions : false
    });


    var el = document.querySelector('canvas');
    el.style.marginLeft = Math.round(-W/2) + 'px';
    el.style.marginTop = Math.round(-H/2) + 'px';


    eng.addLayer('ui');


    eng.loadFont('apache', function() {
        w.ajax('assets/maps/defaults.json', function(err, defaults) {
            if (err) {
                return window.alert(err);
            }

            // level to load
            var mapName = location.hash;
            if (mapName) {
                mapName = mapName.substring(1);
            }
            else {
                mapName = '0';
            }



            var handleItem = function(item0) {
                var item = w.clone(item0);

                if ('template' in item) {
                    w.extend(item, defaults[ item.template ]);
                }
                if ('rotation' in item) {
                    item.rotation *= w.DEG_2_RAD;
                }
                eng.addEntity(item);

                return item0;
            };



            //w.ajax('assets/maps/' + mapName + '.json', function (err, map) {
            w.mapsRepository.get(mapName, function(err, map) {
                if (err) {
                    return window.alert(err);
                }

                var p = map.initialState.pos;
                var r = map.initialState.rotation * w.DEG_2_RAD;

                // load map items
                map.items.forEach(handleItem);

                gui.coords = eng.addText({
                    text  : ['coords: (', p[0], ', ', p[1], ')'].join(''),
                    pos   : [GAP, GAP],
                    font  : 'apache',
                    layer : 'ui'
                });

                gui.selectedId = eng.addText({
                    text  : ['selected: ', lastId].join(''),
                    pos   : [W-GAP, GAP],
                    font  : 'apache',
                    align : 'right',
                    layer : 'ui'
                });

                handleItem({
                    id       : 'car',
                    template : 'car',
                    pos      : p,
                    rotation : r * w.RAD_2_DEG
                });

                inp.calibrate(el);
                eng.centerLayer('default', p);



                inp.on('down', function(_p) {
                    point = new Vec(_p[0], _p[1]);
                    strokeStartScroll = eng.getLayerCenter('default');
                });

                inp.on('move', function(_p) {
                    eng.centerLayer('default', [
                        Math.round( strokeStartScroll[0] - _p[0] + point.x ),
                        Math.round( strokeStartScroll[1] - _p[1] + point.y )
                    ]);
                });

                inp.on('up', function() {
                    var ss = eng.getLayerCenter('default');
                    var t = ['coords: (', ss[0], ', ', ss[1], ')'].join('');
                    eng.updateText(gui.coords, t);
                });



                var getLastEnt = function() {
                    return eng.getEntity(lastId);
                };

                var _getMapEntIdx = function(id) {
                    var ent, i, I, arr = map.items;
                    for (i = 0, I = arr.length; i < I; ++i) {
                        ent = arr[i];
                        if (ent.id === id) {
                            return i;
                        }
                    }
                    return -1;
                };

                var getMapEnt = function(id) {
                    return map.items[ _getMapEntIdx(id) ];
                };

                var deleteMapEnt = function(id) {
                    return map.items.splice( _getMapEntIdx(id) , 1);
                };

                var getCursor = function() {
                    var p0 = eng.getLayerTranslation('default');
                    return [
                        Math.round( point.x - p0[0] ),
                        Math.round( point.y - p0[1] )
                    ];
                };

                var addEnt = function(tplName, ext) {
                    lastId = tplName + '_' + rndBase32(6);

                    var o = {
                        template : tplName,
                        pos      : getCursor(),
                        id       : lastId
                    };
                    if (ext) {
                        w.extend(o, ext, true);
                    }

                    map.items.push( handleItem(o) );
                };

                var removeEnt = function() {
                    if (lastId === 'car') { return; }
                    eng.removeEntity(lastId);
                    deleteMapEnt(lastId);
                    lastId = '';
                };

                var selectEnt = function() {
                    var ent = eng.pickEntity( getCursor() );
                    if (ent) {
                        lastId = ent.__data.id;
                        //console.log('selected ' + lastId);
                        var t = ['selected: ', lastId].join('');
                        eng.updateText(gui.selectedId, t);
                    }
                    else {
                        //console.log('nothing here?'); // TODO?!
                    }
                };

                var rotateEnt = function(rot) {
                    var ent = getLastEnt();
                    var r = eng.getEntityRotation(ent);
                    r += rot * w.DEG_2_RAD;
                    var r2 = Math.round( r * w.RAD_2_DEG );
                    eng.setEntityRotation(ent, r);
                    eng.updateEntitySprite(ent);

                    if (lastId === 'car') {
                        map.initialState.rotation = r2;
                        return;
                    }

                    var entMap = getMapEnt(lastId);
                    entMap.rotation = r2;
                };

                var relocateEnt = function() { // TODO FIX POSITION
                    var ent = getLastEnt();
                    var p = getCursor();
                    eng.setEntityPosition(ent, new Vec(p[0], p[1]) );
                    eng.updateEntitySprite(ent);

                    if (lastId === 'car') {
                        map.initialState.pos = p;
                        return;
                    }

                    var entMap = getMapEnt(lastId);
                    entMap.pos = p;
                };

                var addTiles = function() {
                    var tiles = 'tiles/tile_';
                    //var tiles = w.prompt('tiles per row?', 'tiles/tile_');

                    var tilesPerRow = 15;
                    //var tilesPerRow = w.prompt('tiles per row?', 15);
                    //if (!isFinite(tilesPerRow)) { return; } tilesPerRow = parseInt(tilesPerRow, 10);

                    var x0 = w.prompt('x0', 0);
                    if (!isFinite(x0)) { return; } x0 = parseInt(x0, 10);

                    var y0 = w.prompt('y0', 0);
                    if (!isFinite(y0)) { return; } y0 = parseInt(y0, 10);

                    var ww = w.prompt('w', 1);
                    if (!isFinite(ww)) { return; } ww = parseInt(ww, 10);

                    var hh = w.prompt('h', 1);
                    if (!isFinite(hh)) { return; } hh = parseInt(hh, 10);

                    var L = 2048;

                    w.seq(hh).forEach(function(yi) {
                        var y = yi + y0;
                        w.seq(ww).forEach(function(xi) {
                            var x = xi + x0;
                            var o = {
                                texture: tiles + (x + y*tilesPerRow) + '.png',
                                pos: [ xi*L, yi*L ]
                            };
                            console.log( [x, y].join(', ') );
                            addEnt('tile', o);
                        });
                    });
                };

                var jump = function() {
                    var p = eng.getLayerCenter('default');
                    var x = w.prompt('x', p[0]);
                    if (!isFinite(x)) { return; } x = parseInt(x, 10);
                    var y = w.prompt('y', p[0]);
                    if (!isFinite(y)) { return; } y = parseInt(y, 10);
                    eng.centerLayer('default', [x, y]);
                };

                var load = function() {
                    var name = w.prompt('map name?', '');
                    if (!name) { return; }
                    w.location.hash = name;
                    w.location.reload();
                };

                var save = function() {
                    mapName = w.prompt('map name?', mapName || '');
                    if (!mapName) { return; }

                    //console.log( JSON.stringify(map) );

                    w.mapsRepository.exists(name, function(err, exists) {
                        if (err) {w.alert(err); }

                        var keepSaving = true;
                        if (exists) {
                            keepSaving = w.confirm('Map ' + mapName + ' already exists. Overwrite it?');
                        }

                        if (!keepSaving) {
                            return;
                        }

                        w.mapsRepository.put(mapName, map, function(err, o) {
                            if (err) {w.alert(err); }
                            w.alert(o);
                            if (o === 'saved') {
                                w.location.hash = mapName;
                            }
                        });
                    });
                };



                inp.on('keydown', function(kc) {
                    var name = inp.keyNames[kc] || kc;

                    if (name === 'g') { // add gate
                        addEnt('gate');
                    }
                    else if (name === 'c') { // add cone
                        addEnt('cone');
                    }
                    /*else if (name === 'a') {
                        addEnt('arrow');
                    }*/
                    else if (name === 'm') { // relocate last entity
                        relocateEnt();
                    }
                    else if (name === 'space') { // choose entity
                        selectEnt();
                    }
                    else if (name === 'd') { // delete last entity
                        removeEnt();
                    }
                    else if (name === 't') { // add tiles
                        addTiles();
                    }
                    else if (name === 'left') { // rotate last entity -10 deg
                        rotateEnt(-D_ROT);
                    }
                    else if (name === 'right') { // rotate last entity +10 deg
                        rotateEnt(D_ROT);
                    }
                    else if (name === 'j') { // go places
                        jump();
                    }
                    else if (name === 'l') { // load
                        load();
                    }
                    else if (name === 's') { // save
                        save();
                    }
                    else {
                        //return true;
                    }
                    //console.log(name);
                });
            });
        });
    });

})(this);
