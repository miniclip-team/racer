(function(w) {

    'use strict';

    // 09899 09898
    var CHAR_ON  = '&#10003;';
    var CHAR_OFF = '&#10007;';


    var defaultOptions = {
        music : true,
        sfx   : true
    };


    var eng = getEngine({
        dims  : [1, 1]
    });
    var str = w.store;



    var currentScreen = 'start';
    var selectedMap = '1';



    // what actions do...
    var to = function(screenName) {
        var screen1 = w.QS('#' + currentScreen);
        var screen2 = w.QS('#' + screenName);
        screen1.classList.add('hidden');
        screen2.classList.remove('hidden');
        currentScreen = screenName;

        if (screenName === 'choose-map') {
            var mapsList = w.QS('#maps-list');
            mapsList.innerHTML = '';
            w.mapsRepository.list(function(err, items) {
                if (err) { return w.alert(err); }

                // remove level 0 (it's kind of a template)
                var i = items.indexOf('0');
                if (i !== -1) {
                    items.splice(i, 1);
                }

                // display map names
                items.forEach(function(item) {
                    var el = document.createElement('h2');
                    el.appendChild( document.createTextNode( item ) );
                    el.setAttribute('data-action', 'map-' + item);
                    mapsList.appendChild(el);
                });
            });
        }
    };

    var setToggleValue = function(option, val) {
        var span = w.QS("h2[data-action='toggle-" + option + "'] span");
        span.innerHTML = (val ? CHAR_ON : CHAR_OFF);
    };

    var toggle = function(option) {
        var key = 'option_' + option;
        var val = str.get(key, defaultOptions[option]);
        val = !val;
        str.put(key, val);
        setToggleValue(option, val);

        if (option === 'music') {
            if (val) {
                eng.playAudio('warsaw', {volume:0.3, pan:-0.001, times:1000});
            }
            else {
                eng.stopAudio();
            }
        }
    };

    var play = function(kind) {
        location.href = ['play.html#', selectedMap, ' ', kind].join('');
    };



    // process user clicks into actions
    var onClick = function(ev) {
        ev.preventDefault();
        ev.stopPropagation();

        var el = ev.target;
        if (el.nodeName.toUpperCase() === 'SPAN') { el = el.parentNode; }
        var action = el.getAttribute('data-action');

        if (!action) { return; }

        var cutAt = action.indexOf('-');
        var what = action.substring(0, cutAt);
        var subject = action.substring( cutAt + 1);
        //console.log(what, subject);

        if (what === 'to') {
            to(subject);
        }
        else if (what === 'toggle') {
            toggle(subject);
        }
        else if (what === 'map') {
            selectedMap = subject;
            to('choose-play-mode');
        }
        else if (what === 'play') {
            play(subject);
        }
    };
    document.body.addEventListener('mousedown', onClick);
    document.body.addEventListener('touchend',  onClick);



    // setup starting state of things
    setToggleValue( 'music', str.get('option_music', defaultOptions.music) );
    setToggleValue( 'sfx',   str.get('option_sfx',   defaultOptions.sfx) );



    eng.loadAudio('warsaw', true,  function() {
        if (str.get('option_music', defaultOptions.music)) {
             eng.playAudio('warsaw', {volume:0.3, pan:-0.001, times:1000});
        }
    });

})(this);
