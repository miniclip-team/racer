(function(w) {

    'use strict';



    w.PI = Math.PI;
    w.RAD_2_DEG = 180 / PI;
    w.DEG_2_RAD = PI / 180;

    var Vec = w.SSCD.Vector;



    // returns array from 0 to n-1
    w.seq = function(n) {
        var arr = new Array(n);
        for (var i = 0; i < n; ++i) {
            arr[i] = i;
        }
        return arr;
    };


    w.rndBase32 = function(len) {
        return ( ~~(Math.random() * Math.pow(32, len)) ).toString(32);
    };


    w.clone = function(o) {
        return JSON.parse( JSON.stringify(o) );
    };



    // a - original object, bound to be extended
    // b - reference arguments to copy
    // overrideExisting - if ommitted/falsy, b keys already present in a are not copied
    w.extend = function(a, b, overrideExisting) {
        for (var k in b) {
            if (overrideExisting || (!(k in a))) {
                a[k] = b[k];
            }
        }
        return a;
    };



    // fetches first hit on CSS rule
    w.QS = function(sel) {
        return document.querySelector(sel);
    };



    // ajax to retrieve the JSON map
    w.ajax = function(url, cb) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        var cbInner = function() {
            if (xhr.readyState === 4 && xhr.status > 199 && xhr.status < 300) {
                return cb(null, JSON.parse(xhr.responseText) );
            }
            cb('error requesting ' + url);
        };
        xhr.onload  = cbInner;
        xhr.onerror = cbInner;
        xhr.send(null);
    };

    // supports POST with payload, creds, etc.
    w.ajax2 = function(o) {
        var xhr = new XMLHttpRequest();
        if (o.creds) { xhr.withCredentials = true; }
        xhr.open(o.verb || 'GET', o.url, true);
        var cbInner = function() {
            if (xhr.readyState === 4 && xhr.status > 199 && xhr.status < 300) {
                return o.cb(null, JSON.parse(xhr.response));
            }
            o.cb('error requesting ' + o.url);
        };
        xhr.onload  = cbInner;
        xhr.onerror = cbInner;
        xhr.send(o.payload || null);
    };



    // request animation frame polyfill
    w.raf = (function(){
        return window.requestAnimationFrame       ||
               window.webkitRequestAnimationFrame ||
               window.mozRequestAnimationFrame;
    })();



    // normal and gauss adapted from vorlon.case.edu/~vxl11/software/gauss.html

    /**
     * Generator of pseudo-random number according to a normal distribution with mean=0 and variance=1.
     * Use the Box-Mulder (trigonometric) method and discards one of the two generated random numbers.
     */
    w.normal = function() {
        var u1, u2;
        u1 = u2 = 0.;
        while (u1 * u2 == 0.) {
            u1 = Math.random();
            u2 = Math.random();
        }
        return Math.sqrt(-2. * Math.log(u1)) * Math.cos(2 * Math.PI * u2);
    };


    /**
     * Generator of pseudo-random number according to a normal distribution with given mean and variance.
     * Normalizes the outcome of function normal.
     */
    w.gauss = function(mean, stdev) {
        return stdev * normal() + mean;
    };



    w.randomFloat = function(min, max) {
        return min + (max - min) * Math.random();
    };

    w.randomLinear = function(mean, delta) {
        return mean + ( Math.random() - 0.5 ) * delta;
    };



    // linear interpolation between a and b, with i as parameter, between 0 and 1
    w.lerp = function(a, b, i) {
        return (a*(1-i) + b*i);
    };

    // same as lerp but 2D
    w.lerp2 = function(p1, p2, i) {
        return [
            lerp(p1[0], p2[0], i),
            lerp(p1[1], p2[1], i)
        ];
    };



    w.dotProd = function(v1, v2) {
        return v1.x * v2.x + v1.y * v2.y;
    };

    w.crossProd = function(v1, v2) {
        return v1.x * v2.y - v1.y * v2.x;
    };

    w.compOfAInB = function(vA, vB, bIsNormalized) {
        var n = w.dotProd(vA, vB);
        if (!bIsNormalized) {
            n /= vB.Length();
        }
        return n;
    };

    w.angleBtwAAndB = function(vA, vB) {
        return Math.acos( w.dotProd(vA, vB) / ( vA.len() * vB.len() ) );
    };

    w.normalFromAngle = function(rot) {
        return new Vec(
            Math.cos(rot),
            Math.sin(rot)
        );
    };

    w.sign = function(n) {
        if (n < 0) { return -1; }
        if (n > 0) { return  1; }
        return 0;
    };





    // auxiliary code to rotate shapes in SSCD :)
    w.rotate = function(p, a, toSelf) {
        var ca = Math.cos(a);
        var sa = Math.sin(a);
        var x = ca * p.x - sa * p.y;
        var y = sa * p.x + ca * p.y;
        if (toSelf) {
            p.x = x;
            p.y = y;
            return p;
        }
        return new Vec(x, y);
    };

    w.rotatePoints = function(points, angle, toSelf) {
        if (toSelf) {
            points.forEach(function(p) { w.rotate(p, angle, toSelf); });
            return points;
        }
        return points.map(function(p) { return w.rotate(p, angle); });
    };



    w.bezierCurve = function(t, p0, p1, p2, p3){
        var cX = 3 * (p1.x - p0.x),
            bX = 3 * (p2.x - p1.x) - cX,
            aX =      p3.x - p0.x  - cX - bX,
            cY = 3 * (p1.y - p0.y),
            bY = 3 * (p2.y - p1.y) - cY,
            aY =      p3.y - p0.y  - cY - bY,
            powT2 = t * t,
            powT3 = powT2 * t;

        return new Vec(
            (aX * powT3) + (bX * powT2) + (cX * t) + p0.x,
            (aY * powT3) + (bY * powT2) + (cY * t) + p0.y
        );
    };



    // interpolation functions
    /*
     0   -> 0
     0.5 -> 1
     1   -> 0
     */
    w.pingPong = function(i) {
        return (i > 0.5) ? (2 - 2 * i) : 2 * i;
    };

    w.sine = function(i) {
        return 1 - Math.cos(i * Math.PI / 2);
    };



    // extent SSCD vector interface

    Vec.prototype.set2 = function(x, y) {
        this.x = x;
        this.y = y;
    };

    Vec.prototype.len = function() {
        return Math.sqrt( this.x*this.x + this.y*this.y );
    };

})(this);
