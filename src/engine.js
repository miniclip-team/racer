(function(w) {

    'use strict';


    var PIXI  = w.PIXI;
    var cjsSnd = w.createjs.Sound;



    var SPRITES_PREFIX = 'assets/sprites/';
    var MUSIC_PREFIX = 'assets/music/';
    var SFX_PREFIX = 'assets/sfx/';
    var FONTS_PREFIX = 'assets/fonts/';
    var FONTS_SUFFIX = '.xml';
    var DEFAULT_LAYER = 'default';



    var SSCD = w.SSCD;
    var Vec    = SSCD.Vector;
    var Circle = SSCD.Circle;
    var Line   = SSCD.Line;
    var Strip  = SSCD.LineStrip;



    var FONT_STRINGS = {
        apache: '32px apache'
    };



    var callbacks = {};
    var layers = {};
    var entityLookup = {};
    var spriteLookup = {};

    /**
     * @param {Object} opts
     * @param {Number[2]} [opts.dims]     canvas dimensions in pixels
     * @param {Number}    [opts.gridSize] 2D grid size for collision detection optimization
     * @returns {Object} API
     */
    w.getEngine = function(opts) {

        w.extend(opts, {
            dims            : [512, 512],
            gridSize        : 1024,
            debugCollisions : false
        });

        var dims = opts.dims;

        var stage = new PIXI.Container();

        var addLayer = function(layerName) {
            var layer = new PIXI.Container();
            layers[layerName] = layer;
            stage.addChild(layer);
        };

        addLayer(DEFAULT_LAYER);

        var renderer = PIXI.autoDetectRenderer(dims[0], dims[1]);
        document.body.appendChild(renderer.view);

        w.input.calibrate(renderer.view);


        var WORLD = new SSCD.World({
            grid_size : opts.gridSize
        });
        w.world = WORLD; // TODO TEMP


        /*
         {String}     texture
         {Number[2]}  pos
         {Number}    [tint]
         */
        var addSprite = function (o) {
            var ent = PIXI.Sprite.fromImage(SPRITES_PREFIX + o.texture);
            //ent.blendMode = PIXI.BLEND_MODES.ADD;

            if ('tint' in o) {
                ent.tint = o.tint;
            }

            if ('scale' in o) {
                ent.scale.set(o.scale);
            }

            var anchor = ('anchor' in o) ? o.anchor : 0.5;
            ent.anchor.set(anchor);

            ent.position.x = o.pos[0];
            ent.position.y = o.pos[1];

            if ('rotation' in o) {
                ent.rotation = o.rotation;
            }

            var layer = layers[('layer' in o ? o.layer : DEFAULT_LAYER)];
            layer.addChild(ent);

            return ent;
        };


        var addPolygon = function (o) {
            var g = new PIXI.Graphics();

            g.beginFill(o.fill);

            o.points.forEach(function (p, i) {
                if (i === 0) {
                    g.moveTo(p[0], p[1]);
                }
                else {
                    g.lineTo(p[0], p[1]);
                }
            });
            var p0 = o.points[0];
            g.moveTo(p0[0], p0[1]);
            g.endFill();

            g.cacheAsBitmap = true;

            g.position.x = o.pos[0];
            g.position.y = o.pos[1];

            var layer = layers[('layer' in o ? o.layer : DEFAULT_LAYER)];
            layer.addChild(g);

            return g;
        };


        /*
         {String}     texture          texture name
         {Number[2]}  pos              position

         {Number[2]} [dims]            dimensions - assumes box shape
         {Number}    [radius]          radius     - assumes circle shape
         {Number[2]} [points]          points     - assumes polygon shape
         */
        var addEntity = function (o) {
            if (!o.id) {
                o.id = w.rndBase32(6);
            }

            var spr;
            if ('points' in o && 'fill' in o) {
                spr = addPolygon(o);
            }
            else if ('texture' in o) {
                spr = addSprite(o);
            }

            if ('radius' in o || 'dims' in o || 'points' in o) {

                var aux = {};
                var shape;

                if ('rotation' in o) {
                    aux.rotation = o.rotation;
                }

                var pos = new Vec(o.pos[0], o.pos[1]);

                if ('radius' in o) {
                    shape = new Circle(pos , o.radius);
                }
                else {
                    if ('dims' in o) {
                        var ww = o.dims[0] / 2;
                        var hh = o.dims[1] / 2;
                        o.points = [
                            new Vec(-ww, -hh),
                            new Vec(-ww,  hh),
                            new Vec( ww,  hh),
                            new Vec( ww, -hh)
                        ];
                    }
                    else {
                        o.points = o.points.map(function(p) {
                            return new Vec(p[0], p[1]);
                        });
                    }

                    aux.originalPoints = o.points; // backed up so rotation can be reapplied without cumulative precision errors

                    if ('rotation' in o && o.rotation !== 0) {
                        o.points = w.rotatePoints(o.points, o.rotation);
                    }

                    if (o.points.length === 2) {
                        var c = new Vec(o.pos[0], o.pos[1]);
                        shape = new Line(
                            c.add(o.points[0]).sub_self(pos),
                            c.add(o.points[1]).sub_self(pos)
                        ); // lines won't move for now
                    }
                    else {
                        shape = new Strip(pos, o.points, true);
                    }
                }

                WORLD.add(shape);

                if ('tags' in o) {
                    shape.set_collision_tags(o.tags);
                }

                shape.set_data(aux);

                aux.id     = o.id;
                aux.sprite = spr;

                entityLookup[o.id] = shape;
            }

            if (spr) {
                spriteLookup[o.id] = spr;
            }
        };



        var update = function (t, dt) {
            var cb = callbacks['update'];
            if (cb) {
                cb(t, dt);
            }
        };


        var debugRenderCalled = false;
        var debugCanvas, debugCtx;

        var debugRender = function() {
            if (!debugRenderCalled) {
                debugCanvas = document.createElement('canvas');
                debugCanvas.setAttribute('width',  opts.dims[0]);
                debugCanvas.setAttribute('height', opts.dims[1]);
                document.body.appendChild(debugCanvas);
                debugCtx = debugCanvas.getContext('2d');
                debugRenderCalled = true;
            }

            debugCtx.clearRect(0, 0, opts.dims[0], opts.dims[1]);
            var p = layers['default'].position;
            WORLD.render(debugCanvas, new Vec(p.x, p.y), false, false);
        };


        var prevT = 0 - 1 / 60;
        var loop = function render(t) {
            t /= 1000;
            var dt = t - prevT;

            raf(render);

            update(t, dt);

            renderer.render(stage);

            if (opts.debugCollisions) {
                debugRender();
            }

            prevT = t;
        };


        loop();


        var getEntity = function (id) {
            return entityLookup[id];
        };


        var getSprite = function (id) {
            return spriteLookup[id];
        };

        var removeEntity = function(id) {
            var ent = entityLookup[id];
            var layerName = ent.__data.layer || 'default';
            layers[ layerName].removeChild(ent.__data.sprite);
            delete spriteLookup[id];
            delete entityLookup[id];
        };



        var getEntityPosition = function(ent) {
            return ent.get_position();
        };

        var setEntityPosition = function(ent, p) {
            ent.set_position(p);
        };

        var moveEntity = function(ent, p) {
            ent.move(p);
        };



        var getEntityRotation = function(ent) {
            return ent.__data.rotation || 0;
        };

        var setEntityRotation = function(ent, rot) {
            ent.__data.rotation = rot;
        };

        var spinEntity = function(ent, r) {
            var r0 = ent.__data.rotation || 0;
            ent.__data.rotation = r0 + r;
        };



        var getEntitiesCollidingWith = function(ent, havingTags) {
            var bag = [];
            WORLD.test_collision(ent, havingTags, bag);
            return bag;
        };

        var pickEntity = function(p, havingTags) {
            p = new Vec(p[0], p[1]);
            return WORLD.pick_object(p, havingTags);
        };

        var pickEntities = function(p, havingTags) {
            p = new Vec(p[0], p[1]);
            var bag = [];
            WORLD.test_collision(p, havingTags, bag);
            return bag;
        };

        var repelEntityTouchingEntity = function(entToMove, entAgainst, forceScale) {
            entAgainst.repel(entToMove, 1, forceScale || 1);
        };



        var updateEntitySprite = function(ent) {
            var s = ent.__data.sprite;

            var p = ent.get_position();
            var r = ent.__data.rotation || 0;

            s.position.set(p.x, p.y);
            s.rotation = r;

            var points = ent.__data.originalPoints;

            if (points) {
                ent.__points = w.rotatePoints(points, r);
                ent.__aabb = ent.build_aabb();
            }
        };



        // supports: update, collision, collision-end (relevant for sensors)
        var on = function (evName, cb) {
            callbacks[evName] = cb;
        };


        var setLayerVisibility = function (layerName, isVisible) {
            var layer = layers[layerName];
            layer.visible = isVisible;
        };


        var centerLayer = function (layerName, pos) {
            var layer = layers[layerName];
            layer.position.set(
                -pos[0] + dims[0] / 2,
                -pos[1] + dims[1] / 2
            );
        };

        centerLayer(DEFAULT_LAYER, [0, 0]);

        var getLayerTranslation = function(layerName) {
            var layer = layers[layerName];
            var p = layer.position;
            return [p.x, p.y];
        };

        var getLayerCenter = function(layerName) {
            var layer = layers[layerName];
            var p = layer.position;
            return [
                -p.x + dims[0] / 2,
                -p.y + dims[1] / 2
            ];
        };



        // FONTS

        var loadFont = function (fontName, cb) {
            var loader = PIXI.loader.add(fontName, [FONTS_PREFIX, fontName, FONTS_SUFFIX].join(''));
            if (cb) {
                loader.on('complete', cb);
            }
            loader.load();
        };

        /*
         {String}     text
         {Number[2]}  pos
         {String}     font
         {String}    [align] left|center|right
         */
        var addText = function (o) {
            if (!o.id) {
                o.id = w.rndBase32(6);
            }

            var t = new PIXI.extras.BitmapText(
                o.text,
                {
                    font: FONT_STRINGS[o.font]
                }
            );

            var align = ( ('align' in o) ? o.align : 'left');
            var dx = 0;
            if (align === 'center') {
                dx = t.textWidth / 2;
            }
            else if (align === 'right') {
                dx = t.textWidth;
            }
            t.__originalX = o.pos[0];
            t.__align = align;
            t.position.set(o.pos[0] - dx, o.pos[1]);

            var layer = layers[ ('layer' in o) ? o.layer : DEFAULT_LAYER ];
            layer.addChild(t);

            spriteLookup[o.id] = t;

            return t;
        };

        var updateText = function(t, txt) {
            t.text = txt;
            t.updateText();

            var dx = 0;
            if (t.__align === 'center') {
                dx = t.textWidth / 2;
            }
            else if (t.__align === 'right') {
                dx = t.textWidth;
            }
            t.position.x = t.__originalX - dx;
        };



        // AUDIO


        var supportsMp3 = function() {
            var a = document.createElement('audio');
            return !!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''));
        };

        var audioExtension = supportsMp3() ? '.mp3' : '.ogg';


        //cjsSnd.alternateExtensions = ['mp3'];

        var loadAudio = function (audioName, isMusic, cb) {
            var url = [
                isMusic ? MUSIC_PREFIX : SFX_PREFIX,
                audioName,
                audioExtension
            ].join('');

            if (cb) {
                cjsSnd.on('fileload', function (ev) {
                    if (ev.id !== audioName) {
                        return;
                    }
                    cb(ev);
                });
            }

            cjsSnd.registerSound(url, audioName);
        };

        var playAudio = function (audioName, o) {
            if (opts.mute) { return; }

            if (o === undefined) {
                o = {};
            }
            // interrupt, delay, offset, loop, volume, pan
            cjsSnd.play(
                audioName,
                'none',
                0,
                0,
                ('times' in o) ? o.times - 1 : 0, // ...
                ('volume' in o) ? o.volume : 1, // [0,1]
                ('pan' in o) ? o.pan : 0 // [-1,1]
            );
        };

        var stopAudio = function() {
            cjsSnd.stop();
        };

        var setMuteState = function(isMuted) {
            opts.mute = isMuted;
        };


        return {
            on: on,
            addEntity: addEntity,
            getEntity: getEntity,
            getSprite: getSprite,
            removeEntity: removeEntity,

            // entity transformation ops
            getEntityPosition: getEntityPosition,
            setEntityPosition: setEntityPosition,
            moveEntity: moveEntity,
            getEntityRotation: getEntityRotation,
            setEntityRotation: setEntityRotation,
            spinEntity: spinEntity,
            updateEntitySprite: updateEntitySprite,

            // collision detection
            getEntitiesCollidingWith: getEntitiesCollidingWith,
            pickEntity: pickEntity,
            pickEntities: pickEntities,
            repelEntityTouchingEntity: repelEntityTouchingEntity,

            // layers
            addLayer: addLayer,
            setLayerVisibility: setLayerVisibility,
            centerLayer: centerLayer,
            getLayerCenter: getLayerCenter,
            getLayerTranslation: getLayerTranslation,

            // fonts
            loadFont: loadFont,
            addText: addText,
            updateText: updateText,

            // audio
            loadAudio: loadAudio,
            playAudio: playAudio,
            stopAudio: stopAudio,
            setMuteState: setMuteState
        };
    };

})(this);
