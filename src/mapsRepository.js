(function(w) {
    'use strict';


    var endpoint = 'http://stage.sl.pt/racer-maps/index.php?op=';

    w.mapsRepository = {
        list: function(cb) {
            w.ajax(endpoint + 'list', cb);
        },
        exists: function(id, cb) {
            w.ajax(endpoint + 'exists&id=' + id, cb);
        },
        get: function(id, cb) {
            w.ajax(endpoint + 'load&id=' + id, cb);
        },
        put: function(id, o, cb) {
            var data = JSON.stringify(o);
            w.ajax2({
                url     : endpoint + 'save&id=' + id,
                verb    : 'POST',
                payload : data,
                cb      : cb
            });
        }
    };

})(this);
