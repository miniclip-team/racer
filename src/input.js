(function(w) {
    'use strict';


    var callbacks = {};


    /*
      keyboard callbacks return a keycode
      mouse/touch callbacks return a pair of x,y coords
      iif callback handler returns trueish, events don't get stopped, otherwise they do.
      use calibrate to set the coords origin to the given element
     */



    // KEYBOARD
    var keysDown = {}; // keys which are down

    var onKeyDown = function(ev) {
        if (ev.shiftKey || ev.altKey || ev.ctrlKey || ev.metaKey) { return; }
        var kc = ev.keyCode;
        keysDown[kc] = true;

        var cb = callbacks['keydown'];
        var letItPass = false;
        if (cb) {
            letItPass = cb(kc);
        }
        if (letItPass) { return; }

        ev.preventDefault();
        ev.stopPropagation();
    };

    var onKeyUp = function(ev) {
        if (ev.shiftKey || ev.altKey || ev.ctrlKey || ev.metaKey) { return; }
        var kc = ev.keyCode;
        keysDown[kc] = false;

        var cb = callbacks['keyup'];
        var letItPass = false;
        if (cb) {
            letItPass = cb(kc);
        }
        if (letItPass) { return; }

        ev.preventDefault();
        ev.stopPropagation();
    };

    var keys = {};
    var keyNames = {};

    [
        ['up', 38],
        ['down', 40],
        ['left', 37],
        ['right', 39],
        ['space', 32],
        ['enter', 13],
        ['escape', 27],
        ['backspace', 8],
        ['0', 48],
        ['1', 49],
        ['2', 50],
        ['3', 51],
        ['4', 52],
        ['5', 53],
        ['6', 54],
        ['7', 55],
        ['8', 56],
        ['9', 57],
        ['q', 81],
        ['w', 87],
        ['e', 69],
        ['r', 82],
        ['t', 84],
        ['y', 89],
        ['u', 85],
        ['i', 73],
        ['o', 79],
        ['p', 80],
        ['a', 65],
        ['s', 83],
        ['d', 68],
        ['f', 70],
        ['g', 71],
        ['h', 72],
        ['j', 74],
        ['k', 75],
        ['l', 76],
        ['z', 90],
        ['x', 88],
        ['c', 67],
        ['v', 86],
        ['b', 66],
        ['n', 78],
        ['m', 77],
        ['f1', 112],
        ['f2', 113],
        ['f3', 114],
        ['f4', 115],
        ['f5', 116],
        ['f6', 117],
        ['f7', 118],
        ['f8', 119],
        ['f9', 120],
        ['f10', 121],
        ['f11', 122],
        ['f12', 123]
    ].forEach(function(pair) {
        var name = pair[0];
        var kc = pair[1];
        keys[name] = kc;
        keyNames[kc] = name;
    });

    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup',   onKeyUp);



    // MOUSE/TOUCH
    var isDown = false;
    var justGotDown = false;
    var justGotUp = false;
    var p0 = [0, 0]; // used to transform mouse/touch coords to chosen origin



    // returns mouse/touch event's coordinates
    var unwrapEvent = function(ev) {
        ev = (ev.changedTouches && ev.changedTouches[0]) ? ev.changedTouches[0] : ev;
        return [
            ev.pageX - p0[0],
            ev.pageY - p0[1]
        ];
    };



    var onDown = function(ev) {
        var cb = callbacks['down'];
        var letItPass = false;
        if (cb) {
            var pos = unwrapEvent(ev);
            letItPass = cb(pos);
        }

        isDown = true;
        justGotDown = true;

        if (letItPass) { return; }

        ev.preventDefault();
        ev.stopPropagation();
    };

    var onMove = function(ev) {
        justGotDown = false;
        justGotUp   = false;

        if (!isDown) { return; }

        var cb = callbacks['move'];
        var letItPass = false;
        if (cb) {
            var pos = unwrapEvent(ev);
            letItPass = cb(pos);
        }

        if (letItPass) { return; }

        ev.preventDefault();
        ev.stopPropagation();
    };

    var onUp = function(ev) {
        var cb = callbacks['up'];
        var letItPass = false;
        if (cb) {
            var pos = unwrapEvent(ev);
            letItPass = cb(pos);
        }

        isDown = false;
        justGotUp = true;

        if (letItPass) { return; }

        ev.preventDefault();
        ev.stopPropagation();
    };

    window.addEventListener('mousedown',  onDown);
    window.addEventListener('touchstart', onDown);
    window.addEventListener('mousemove',  onMove);
    window.addEventListener('touchmove',  onMove);
    window.addEventListener('mouseup',    onUp);
    window.addEventListener('touchend',   onUp);



    w.input = {
        calibrate: function(el) {
            var r = el.getBoundingClientRect();
            p0 = [r.left, r.top];
        },
        on: function(evName, cb) { // down, up, keydown, keyup
            callbacks[evName] = cb;
        },
        isDown: function() {
            return isDown;
        },
        justGotDown: function() {
            return justGotDown;
        },
        justGotUp: function() {
            return justGotUp;
        },
        isKeyDown: function(kc) {
            return keysDown[kc];
        },
        keys: keys,
        keyNames: keyNames
    };


})(this);
