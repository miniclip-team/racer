(function(w) {

    'use strict';



    // imports and constants
    var Vec = w.SSCD.Vector;
    var inp = w.input;

    var W = window.innerWidth;
    var H = window.innerHeight;

    var GAP = 16;
    var NUM_DOTS = 8;




    // configuration - '<mapName> <nrPlayers>'
    var config = location.hash || '#1_straight 1';
    config = decodeURIComponent( config.substring(1) ); // get rid of hash char and any escaping chars
    config = config.split(' ');
    var mapName = config[0];
    var nrPlayers = parseInt(config[1], 10);
    var timePerTurn = 15;



    var p0, p1; // stroke ends
    var carIsRunning = false;
    var powerN, powerT; // forces to apply to the current car
    var timeLeft = timePerTurn;


    var createPlayerState = function() {
        return {
            foundGates: {},
            history: [],
            score: 0,
            getLast: function() {
                return this.history[ this.history.length - 1 ];
            }
        }
    };

    var currentPlayerIdx = 0;
    var playerStates = w.seq(nrPlayers).map(createPlayerState);
    var currentPlayerState = playerStates[currentPlayerIdx];

    var nrGatesInMap = 0; // will be updated on map load



    var gui = {};

    var scoreIncreaseSingle = function(eventName, val) {
        if      (eventName === 'new-gate') { return 100; }
        else if (eventName === 'collision') { return -20; }
        else if (eventName === 'distance-traveled') { return Math.round( val / 10 ); }
        return 0;
    };

    var scoreIncreaseMulti = function(eventName, val) {
        return ( (eventName === 'new-gate') ? 1 : 0);
    };

    var swapCarsRenderingOrder = function() {
        if (nrPlayers < 2) { return; }
        var car0 = eng.getSprite('car_0');
        var car1 = eng.getSprite('car_1');
        car0.parent.swapChildren(car0, car1);
    };

    var leave = function() {
        location.href = 'index.html';
    };

    var replay = function() {
        location.reload(true);
    };

    var scoreIncrease = ( (nrPlayers === 1) ? scoreIncreaseSingle : scoreIncreaseMulti);




    // spawns game engine
    var eng = getEngine({
        dims            : [W, H],
        gridSize        : 1024,
        debugCollisions : false,
        mute            : !w.store.get('option_sfx', true)
    });


    // canvas dimensions and layers
    var el = document.querySelector('canvas');
    el.style.marginLeft = Math.round(-W/2) + 'px';
    el.style.marginTop = Math.round(-H/2) + 'px';

    eng.addLayer('ui');
    eng.addLayer('stroke');



    // load additional assets ahead of time...
    var textures = {
        gate0  : PIXI.Texture.fromImage('assets/sprites/gate0.png'),
        gate1  : PIXI.Texture.fromImage('assets/sprites/gate1.png'),
        gate01 : PIXI.Texture.fromImage('assets/sprites/gate01.png')
    };
    eng.loadAudio('checkpoint');
    eng.loadAudio('complete');
    eng.loadAudio('cone_hit');
    eng.loadAudio('tick');
    eng.loadAudio('no_time_left');
    eng.loadAudio('elastic');

    eng.loadFont('apache', function() {
        w.ajax('assets/maps/defaults.json', function(err, defaults) {
            if (err) {
                return window.alert(err);
            }



            // converts map items into engine entities (that is, applies template defaults and converts degrees to rads)
            var handleItem = function(item0) {
                var item = w.clone(item0);

                if ('template' in item) {
                    w.extend(item, defaults[ item.template ]);
                }
                if ('rotation' in item) {
                    item.rotation *= w.DEG_2_RAD;
                }
                eng.addEntity(item);

                return item0;
            };



            // fetches map from repository
            w.mapsRepository.get(mapName, function(err, map) {
                if (err) {
                    return window.alert(err);
                }

                // load map items
                map.items.forEach(handleItem);

                // counts gates in map to update nrGatesToFind
                var gatesInMap = map.items.filter(function(item) { return item.template === 'gate' });
                gatesInMap.forEach(function(g) {
                    g = eng.getEntity(g.id);
                    g.__data.playersWhoCrossed = {};
                });
                nrGatesInMap = gatesInMap.length;



                // place GUI parts
                gui.gates = eng.addText({
                    text  : 'GATES: 0/' + nrGatesInMap,
                    pos   : [GAP, GAP],
                    font  : 'apache',
                    layer : 'ui'
                });

                gui.currentPlayer = eng.addText({
                    text  : 'PLAYER ' + (currentPlayerIdx + 1),
                    pos   : [W/2, GAP],
                    font  : 'apache',
                    layer : 'ui',
                    align : 'center'
                });
                if (nrPlayers < 2) {
                    gui.currentPlayer.alpha = 0;
                }

                gui.score = eng.addText({
                    text  : 'SCORE: ' + currentPlayerState.score,
                    pos   : [W-GAP, GAP],
                    font  : 'apache',
                    layer : 'ui',
                    align : 'right'
                });
                if (nrPlayers > 1) {
                    gui.score.alpha = 0;
                }

                if (nrPlayers > 1) {
                    gui.timeLeft = eng.addText({
                        text  : 'TIME LEFT: ' + (timeLeft).toFixed(2),
                        pos   : [W-GAP, GAP],
                        font  : 'apache',
                        layer : 'ui',
                        align : 'right'
                    });
                }

                gui.leave = eng.addText({
                    text  : 'LEAVE GAME',
                    pos   : [GAP, H-GAP-32],
                    font  : 'apache',
                    layer : 'ui'
                });



                var p = map.initialState.pos;
                var r = map.initialState.rotation * w.DEG_2_RAD;

                eng.centerLayer('default', p);

                playerStates.forEach(function(ps, i) { // place cars and player states at start pos/rot
                    ps.history.push({
                        p : new Vec(p[0], p[1]),
                        r : r
                    });

                    handleItem({
                        id       : 'car_' + i,
                        texture  : 'car' + i + '.png',
                        template : 'car',
                        pos      : p,
                        rotation : r * w.RAD_2_DEG
                    });

                    if (i !== 0) {
                        eng.getSprite('car_' + i).alpha = 0.25;
                    }
                });

                swapCarsRenderingOrder();


                // load ui items (red dot overlays on stroke layer)
                w.seq(NUM_DOTS).forEach(function(i) {
                    handleItem({
                        id      : 'dot' + i,
                        template: 'dot'
                    });
                });

                eng.setLayerVisibility('stroke', false);



                var switchPlayer = function() {
                    carIsRunning = false;
                    ++currentPlayerIdx;
                    if (currentPlayerIdx >= nrPlayers) { currentPlayerIdx = 0; }

                    eng.getSprite('car_' + currentPlayerIdx).alpha = 1;

                    currentPlayerState = playerStates[currentPlayerIdx];
                    eng.updateText(gui.gates, ['GATES: ', Object.keys( currentPlayerState.foundGates ).length, '/', nrGatesInMap].join('') );
                    eng.updateText(gui.currentPlayer, 'PLAYER ' + (currentPlayerIdx + 1) );
                    eng.updateText(gui.score, 'SCORE: ' + currentPlayerState.score);
                    var pPrev = currentPlayerState.getLast().p;
                    eng.centerLayer('default', [pPrev.x, pPrev.y]);

                    if (nrPlayers > 1) {
                        timeLeft = timePerTurn;
                        gui.timeLeft.tint = 0xFFFFFF;

                        swapCarsRenderingOrder();
                    }
                };



                eng.on('update', function(t, dt) {
                    var history = currentPlayerState.history;

                    var car = eng.getEntity('car_' + currentPlayerIdx);

                    if (carIsRunning) {
                        if (!w.t0) {
                            //console.log('START\npowerN: ' + powerN.toFixed(1) + '\npowerT: ' + powerT.toFixed(1) );
                            eng.playAudio('elastic');
                            w.t0 = t;
                        }

                        var dur = 1;

                        var obstacles = eng.getEntitiesCollidingWith(car);
                        //console.log(t);
                        obstacles.forEach(function(ent) {
                            var id = ent.__data.id;

                            if (id.indexOf('car_') === 0) { return; } // skip collisions between cars

                            //console.log(id);

                            if (id.indexOf('gate_') !== 0) { // collision against cone, repeat turn...
                                w.t0 = t - dur;
                                eng.playAudio('cone_hit');
                                //console.log('BAM -> BACK');

                                // back to prev pos (if last was failure, repeat!)
                                var repeatedCollision = (
                                    currentPlayerState.lastBackup &&
                                    currentPlayerState.lastBackup.r === currentPlayerState.getLast().r
                                );

                                if (repeatedCollision) {
                                    //console.log('REPEATED COLLISION -> BACK AGAIN');
                                    currentPlayerState.history.pop();
                                }

                                var st = currentPlayerState.history.pop();

                                currentPlayerState.lastBackup = st;

                                eng.setEntityPosition(car, st.p);
                                eng.setEntityRotation(car, st.r);
                                eng.updateEntitySprite(car);

                                currentPlayerState.score += scoreIncrease('collision');
                            }
                            else if (!currentPlayerState.foundGates[id]) { // new gate crossed
                                //console.log('found gate ' + id);
                                currentPlayerState.foundGates[id] = true;

                                // update who crossed it and change texture accordingly
                                ent.__data.playersWhoCrossed[ currentPlayerIdx ] = true;
                                var who = Object.keys(ent.__data.playersWhoCrossed);
                                who.sort();
                                ent.__data.sprite.texture = textures[ 'gate' + who.join('') ];

                                currentPlayerState.score += scoreIncrease('new-gate');

                                var nrGatesFound = Object.keys( currentPlayerState.foundGates ).length;
                                eng.updateText(gui.gates, ['GATES: ', nrGatesFound, '/', nrGatesInMap].join('') );

                                if (nrGatesFound === nrGatesInMap) {
                                    eng.playAudio('complete');
                                    // TODO ANIM AND QUESTION
                                    setTimeout(function() {
                                        if (w.confirm('player ' + (currentPlayerIdx + 1) + ' wins!\nplay again?')) {
                                            replay();
                                        }
                                        else {
                                            leave();
                                        }
                                    }, 1000);
                                }
                                else {
                                    eng.playAudio('checkpoint');
                                }
                            }

                            eng.updateText(gui.score, 'SCORE: ' + currentPlayerState.score);
                        });
                        //console.log(obstacles);

                        var tt = t - w.t0;
                        var i = w.pingPong(1 - tt/dur); // pyramid 0 -> 1 -> 0
                        var n = w.normalFromAngle( currentPlayerState.getLast().r  );

                        // transform car
                        var dPos = n.multiply_scalar_self( powerN * dt * 4 * i );


                        //var dRot = powerT * dt * -i * 0.025;
                        var dRot = -i * ((i * 20) + powerN) * powerT * 0.000002; // component based solely on torque, part scaled with linear velocity

                        eng.moveEntity(car, dPos);
                        eng.spinEntity(car, dRot);
                        eng.updateEntitySprite(car);

                        var p = eng.getEntityPosition(car);
                        var r = eng.getEntityRotation(car);

                        inp.calibrate(el);
                        eng.centerLayer('default', [p.x, p.y]);

                        if (tt >= dur) {
                            //console.log('STOP');
                            powerN = 0;
                            powerT = 0;
                            delete w.t0;

                            var pPrev = currentPlayerState.getLast().p;
                            var dist = SSCD.Math.distance(p, pPrev);
                            history.push({p:p, r:r});
                            currentPlayerState.score += scoreIncrease('distance-travelled', dist);

                            eng.getSprite('car_' + currentPlayerIdx).alpha = 0.25;

                            switchPlayer();

                            return;
                        }

                        //console.log('tt', tt.toFixed(2) );
                    }
                    else if (nrPlayers > 1) { // CAR ISN'T YET RUNNING, DECREMENTING TIMER

                        if ( Math.floor(timeLeft) !== Math.floor(timeLeft - dt) ) {
                            eng.playAudio('tick', {volume:(1 - (timeLeft / timePerTurn))});
                        }

                        if (timeLeft > 3 && timeLeft - dt < 3) {
                            gui.timeLeft.tint = 0xFF0000;
                        }

                        timeLeft -= dt;

                        if (timeLeft < 0) {
                            eng.playAudio('no_time_left');
                            return switchPlayer();
                        }

                        eng.updateText(gui.timeLeft, 'TIME LEFT: ' + (timeLeft).toFixed(2));
                    }
                });

                inp.on('down', function(p) {
                    //console.log('down', p.join(' , '));

                    if (p[0] < 180 && p[1] > H - 64) {
                        if (w.confirm('leave game?')) {
                            leave();
                        }
                        return;
                    }

                    if (!carIsRunning) {
                        p0 = new Vec(p[0], p[1]);

                        w.seq(NUM_DOTS).forEach(function(i) {
                            var e = eng.getSprite('dot' + i);
                            e.position.set(W/2, H/2);
                        });

                        eng.setLayerVisibility('stroke', true);
                    }
                });

                inp.on('move', function(p) {
                    //console.log('move', p.join(' , '));

                    if (carIsRunning || !p0) { return; }

                    p1 = new Vec(p[0], p[1]);

                    // v is the vector the user stroked
                    var v = p0.sub(p1);

                    // normal of car direction
                    var n = w.normalFromAngle( currentPlayerState.getLast().r );

                    var lenVInN = compOfAInB(v, n, true);

                    // component of v in the direction on -n
                    var vN = n.multiply_scalar(lenVInN);

                    var vT = v.sub(vN);

                    vN.multiply_scalar_self(-1);
                    vT.multiply_scalar_self(-1);

                    // detect reverse
                    var angleBtwNV = w.angleBtwAAndB(n, v) * w.RAD_2_DEG;
                    var torqueSign = Math.abs(angleBtwNV) > 90 ? -1 : 1;
                    //console.log('N^V', angleBtwNV, 'TSIGN', torqueSign);

                    var cp = w.crossProd(vN, vT);
                    powerN = lenVInN;
                    powerT = torqueSign * w.sign(cp) * vT.len();

                    // slow down reverse
                    if (torqueSign === -1) {
                        powerN *= 0.66;
                    }

                    //console.log('vN', vN.x, vN.y);
                    //console.log('vT', vT.x, vT.y);

                    var a = new Vec(W/2, H/2);
                    var b = a.add(vN);
                    var c = b.add(vT);

                    w.seq(NUM_DOTS).forEach(function(i) {
                        var e = eng.getSprite('dot' + i);
                        i = (i + 1) * 0.1;
                        var p = bezierCurve(i, a, b, b, c);
                        e.position.set(p.x, p.y);
                    });
                });

                inp.on('up', function(p) {
                    //console.log('up  ', p.join(' , '));

                    if (carIsRunning) { return; }

                    p0 = undefined;
                    p1 = undefined;

                    eng.setLayerVisibility('stroke', false);

                    // invalidate small strokes
                    if (Math.abs(powerN) > 10) {
                        carIsRunning = true;
                    }
                });
            });
        });
    });

})(this);
