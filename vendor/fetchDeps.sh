#!/usr/bin/env bash

wget -O pixi.js     https://cdn.rawgit.com/GoodBoyDigital/pixi.js/dev/bin/pixi.js
wget -O pixi.min.js https://cdn.rawgit.com/GoodBoyDigital/pixi.js/dev/bin/pixi.min.js

#wget -O box2d.js     https://raw.githubusercontent.com/hecht-software/box2dweb/master/Box2D.js
#wget -O box2d.min.js https://raw.githubusercontent.com/hecht-software/box2dweb/master/Box2d.min.js

wget -O sscd.js     https://raw.githubusercontent.com/RonenNess/SSCD.js/master/dist/sscd.1.2.js
wget -O sscd.min.js https://raw.githubusercontent.com/RonenNess/SSCD.js/master/dist/sscd.1.2.min.js

#wget -O soundjs.js     https://code.createjs.com/soundjs-0.6.1.js
wget -O soundjs.min.js https://code.createjs.com/soundjs-0.6.1.min.js
