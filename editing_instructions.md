# how to create a map

## 1) choose bg

Choose the subset of available tiles you need to spawn as background imagery.

Use this as reference <http://edit.divshot.io/map.html>.

Example: you want to race around Miniclip HQ.  
Using the map page you can inspect that's located at 6,9.  
You want to start from the nearby roundabout so you must include it and the road between those.  
In the end you need to spawn at least tiles ranging from 6,8 to 7,9.  
That's a square of 2x2 starting from 6,8 (top-left tile).


## 2) invoke the tiles

Fire the editor at <http://edit.divshot.io/edit.html>.  
Now we'll spawn the background tiles.
You should do this first in order for the remaining entities to stand above the tiles
and to make sure your entities are aligned with roads and such.
press `t`

answer these:

* x0 - 6
* y0 - 8
* w - 2
* h - 2

You should now have a 2 x 2 tile set starting from the 0,0 coords.


## 3) define start

You should now define the car initial location and orientation.
To do so you should select the car (that's the default selected entity) by clicking on it and pressing `space`.  
Now scroll to where you want the car to be, click it and press `m`. the car should now be correctly placed.  
Now use the `left` and `right` arrow keys to adjust car orientation.


## 4) place the gates

So far the map requires nothing to complete.  
You should now imagine a track and distribute gates throughout its path using `g`.  
The order is not relevant. Just try orienting them tangent to the path.  
To do so use the `left` and `right` arrow keys to adjust the orientation of each gate spawned.  
Nothing stops you from not using the road in the tiles. Make sure the path is obvious though :)


## 5) place some cones (optional)

Placing cones is completely optional, though I find them useful, since they limit the regions the car can safely drive to  
and enhance the visibility of the track. Place cones at your will using `c`.


## 6) save the map

When you're happy with the track result, press the `s` key.  
You should find the JSON representation of the track on the developer console.  
Copy it entirely to a JSON file, save it on the `assets/maps` directory and you're done.
To play it, open the `play.html` with the hash being the file name, ex: `play.html#awesome_track`


## user interface reference

performing a mouse/touch drag scrolls the map

`t` - spawns tiles
`c` - spawns a new *c*one
`g` - spawns a new *g*ate
`space` - chooses element below last click/touch
`m` - moves selected element to last click/touch
`j` - jumps to given coords (useful to navigate large maps)
`left` and `right` arrows - rotates selected element
`l` - loads the map from the public repository
`s` - saves the map in the public repository
