# City Drifter



## APPROACH

* pixi to render
* box2D to simulate world physics, nope, SSCD and handmade dynamics!
* screens as web pages
* 2 pixi layers: map and ui
* camera directing is just layer movement
* idea! for the map scrap maps nearby Miniclip Studios Tagus Park! :)
* why not post maps online? not that hard
* point system for playing alone, timer for head to head



## plan of tasks, mostly chronologically ordered

* ~~abstract rendering, physics, UI~~
* ~~play music, sfx~~
* ~~draw font~~
* ~~basic move mechanics~~
* ~~fix physics coords and repel~~
* ~~gate completion logic and scores~~
* ~~cone collision gives score penalty~~
* ~~map background - had a fabulous idea! use google maps custom tiles as BG! implemented solution on the other/tiles dir~~ 
* ~~basic map (add proper gates and cones)~~
* camera directing (some inertia at least)
* assets
    * music
        * ~~splash/menus - loop a small part of warsaw~~
    * sfx
        * ~~crash against cone~~
        * ~~gate completion~~
        * ~~race completion~~
        * ~~car impulse (elastic)~~
        * car drifting
        * car engine (dynamic)
        
    * sprites
        * ~~car~~
        * ~~cone~~
        * ~~gate~~

* screens design
    * ~~splash screen~~
    * main menu
        * ~~options~~
        * ~~play (single or two-player hot seat)~~
        
    * options
        * ~~music on/off~~
        * ~~sfx on/off~~

* other nice things...
    * ~~use remote map repository instead of local directory (took me a couple of minutes, easier to save a map now, used [this](https://github.com/JosePedroDias/phpKeyValueStore))~~
    * ~~leave game button (bottom left)~~
    * ~~points system only makes sense on SINGLE PLAYER, on two players, replace with timer~~
    * ~~encapsulate player in order to allow 2 players, implement 2P~~
    * ~~play timer? when 5 seconds, red. when 0, skip turn~~
    * ~~gates show who completed them in 2P mode~~
    * ~~two consecutive fails -> auto undo~~
    * how to play (animated gif / video with thumb, car and resulting movement)
    * high scores
    * credits
    * end game animation (transition to black, animate text scale and opacity?, play again | back to main menu)
    * reflow the screen on resize?

    
* what could be done from here on

    * a mini map overlay
    * different cars with car handling properties and skins (top speed, steering capabilities) -> IAP!
    * lighting effects could be applied via normal mapping the entity sprites to make the gfx pop
    * dynamic entities could be added:
        * people crossing the street
        * gates which would periodically open and close 
    * the 2 player mode is a strong candidate for playing remotely with a simple lobby to pair people without opponents
    * skid marks (at tyre positions when tangential velocity above threshold
    * big drift - overlay with mad max guitarist doing a riff



## REFERENCE

### pixi

* [API](http://pixijs.github.io/docs/)
* [examples](http://pixijs.github.io/examples/index.html)
    
    
### box2d
   
* [api](http://www.box2dflash.org/docs/2.1a/reference/)
* [walkthrough](http://blog.sethladd.com/2011/09/box2d-javascript-example-walkthrough.html)
* [joints](http://blog.sethladd.com/2011/09/box2d-and-joints-for-javascript.html)
* [impulse](http://blog.sethladd.com/2011/09/box2d-impulse-and-javascript.html)
* [collision](http://blog.sethladd.com/2011/09/box2d-collision-damage-for-javascript.html)
* [polygons](http://blog.sethladd.com/2011/09/box2d-and-polygons-for-javascript.html)


### soundjs

* [code](https://code.createjs.com/)
* [api](http://www.createjs.com/docs/soundjs/modules/SoundJS.html)



## CREDITS


### GFX

* [car.png](http://images.clipartpanda.com/car-top-view-clipart-red-racing-car-top-view-fe3a.png)
* [red-dot.png](http://static.moviegolf.com/img/red-dot.png)
* [cone.png](http://thumb7.shutterstock.com/thumb_large/1213370/114247099/stock-photo-three-orange-highway-traffic-cones-with-white-stripes-top-view-isolated-on-white-background-114247099.jpg)
* [tarmac_bg](https://flic.kr/p/oDG5Sh)
* [arrow](http://www.google.pt/imgres?imgurl=https://www.colourbox.com/preview/6250951-the-white-arrow-on-the-road-background.jpg&imgrefurl=https://www.colourbox.com/image/the-white-arrow-on-the-road-background-image-6250951&h=531&w=800&tbnid=9ifj5pobjrMAaM:&docid=nJ9FLX75hSUjbM&ei=rd4FVoTzBIGZab6qq9AO&tbm=isch&ved=0CFUQMygwMDBqFQoTCMT9kYixk8gCFYFMGgodPtUK6g)
* [arrow_l](http://www.google.pt/imgres?imgurl=http://therapyevanston.com/wp-content/uploads/2011/04/dreamstimefree_1131153-arrow-turn-left.jpg&imgrefurl=http://therapyevanston.com/2012/11/midlife-and-the-road-not-taken/&h=2908&w=1939&tbnid=QyjP7nHj-s8G6M:&docid=9Q1aNauiSEFN2M&ei=rd4FVoTzBIGZab6qq9AO&tbm=isch&ved=0CCEQMygEMARqFQoTCMT9kYixk8gCFYFMGgodPtUK6g)


### AUDIO

* [warsaw](http://freemusicarchive.org/music/Groove_Armada/James_Curd_-_Greenskeepers_-_Ziggy_Franklin_-_Studio_11_sampler/Warsaw_James_Curd_Remix)
* [cone_hit](http://www.freesound.org/people/bellick/sounds/151468/)
* [checkpoint](http://www.freesound.org/people/LittleRobotSoundFactory/sounds/274183/)
* [complete](http://www.freesound.org/people/LittleRobotSoundFactory/sounds/270404/)
* [tick](http://freesound.org/people/FoolBoyMedia/sounds/264498/)
* [no_time_left](http://freesound.org/people/pm_b/sounds/271088/)
* [elastic](http://freesound.org/people/Corsica_S/sounds/148078/)


### FONTS

* [SF Archery Black](http://www.fontsquirrel.com/fonts/SF-Archery-Black)


### OTHER

* [CSS Shake](http://elrumordelaluz.github.io/csshake/)



## SOFTWARE USED

* [gimp](http://www.gimp.org/) - sprite artwork editing
* [audacity](http://audacityteam.org/) - sound editing and encoding
* [littera](http://kvazars.com/littera/) - font sprite generation
* [node.js](https://nodejs.org/en/) and [imagemagick](http://www.imagemagick.org/) - download and stitching of tiles



## TO SERVE TEMPORARILY

    divshot deploy production

<http://edit.divshot.io/>



------------


# Some explaining now...

Since I'm very experienced in JavaScript development and I'm familiar with the APIs I used here,
opted for doing it for modern browsers. Whenever possible, I used an approach I could as easily
apply to the flash runtime, both in terms of abstractions available and performance.
I tried to keep the input and layout compatible with mobile devices.


## Technology used

I used PIXI to render to canvas via WebGL, with fallback to regular canvas operations.
It offers a node graph with features very much like Flash does.

For a couple of days I tried to rely on Box2D for both collisions and dynamics, but ended up fighting the physics
side-effects so I decided to ditch it in favor of a very simple collision detection lib (SSCD) and compute the dynamics
myself (more control!).

Used soundjs to abstract audio playback. Not doing anything fancy here - just preload the audio used and fire the playback
when relevant. The menu song loops.

I ended up abstracting most of PIXI, SSCD and SoundJS in something I called the engine. This abstraction was very helpful
when I ditch Box2D since not much of the engine API changed with the switch.


## Tiles?

Since I had a short time to build the game, had the crazy idea of using the map of a real location as backdrop for the
levels. I know a bit of mapping systems, having worked with leaflet maps with several layers, openstreetmaps, etc.
Ended up finding a way of customizing the overlays google maps present (to disable most labels and icons and enhance the roads).
Then scripted a bit to automate both the downloading of individual tiles, merging without attribution and retile in gfx. cards
friendly 256x256 tiles. For the kicks I chose the surroundings of Miniclip Studio HQ on Tagus Park.


## Code structure

I used the HTML page abstraction to work somewhat as different stages.
The menu is provided by the `index` page, playing a level occurs in the `play` page and there are a couple of additional
pages I created to aid me in the process of creating levels
* the `edit` page allows editing game entities, spawning new ones, scrolling the scene by hand and saving the resulting level;
* the `map` page exists to give an overview of tile tiles universe I have set up, so I can choose which tiles to apply to each level - more on this later.

Abstracted other aspects in very simple solutions:
`aux` is a bag of common functions I tend to use;
`input` abstracts mouse and touch events to a common api, along with keyboard events (these I only used in the level editor);
the `store` is a facade over localStorage and `mapsRepository` came very late in development, when I noticed that it would rock to
have a centralized map repository - I was loading levels with AJAX already and saving them via developer tools export was cumbersome.

Most libraries here are being shared via the global window object, as clojures, hiding internal structures. All are exposing
a stateless API, expect for engine, which exports a factory receiving parameters to output its API. It should be all quite
self-explanatory. I've used SSCD's Vector class to do my own calculations.
